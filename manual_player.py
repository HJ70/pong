from player import Player


class ManualPlayer(Player):

    def __init__(self, wnd, environment, paddle, up_key, down_key,
                 use_mouse=False):
        super().__init__(environment, lambda y: paddle.sety(y))
        self.step = 20
        self.wnd = wnd
        self.paddle = paddle
        wnd.onkeypress(self.up, up_key)
        wnd.onkeypress(self.down, down_key)
        if use_mouse:
            self.onmove(wnd, self.move_handler)

    def up(self):
        self.paddle_sety_func(self.paddle.ycor() + self.step)

    def down(self):
        self.paddle_sety_func(self.paddle.ycor() - self.step)

    @staticmethod
    def onmove(wnd, fun, add=None):
        """
        Bind fun to mouse-motion event on screen.

        Arguments:
        self -- the singular screen instance
        fun  -- a function with two arguments, the coordinates
            of the mouse cursor on the canvas.
        """

        if fun is None:
            wnd.cv.unbind('<Motion>')
        else:
            def eventfun(event):
                fun(wnd.cv.canvasx(event.x) / wnd.xscale,
                    -wnd.cv.canvasy(event.y) / wnd.yscale)

            wnd.cv.bind('<Motion>', eventfun, add)

    # noinspection PyUnusedLocal
    def move_handler(self, x, y):
        self.onmove(self.wnd, None)
        self.paddle.goto(self.paddle.xcor(), y)
        self.onmove(self.wnd, self.move_handler)
