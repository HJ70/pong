import turtle

PADDLE_HEIGHT = 100
PADDLE_WIDTH = 20
PADDLE_HEIGHT_HALF = PADDLE_HEIGHT / 2
PADDLE_WIDTH_HALF = PADDLE_WIDTH / 2


class PaddleBorders:

    def __init__(self, paddle):
        self.left = paddle.xcor() - PADDLE_WIDTH_HALF
        self.right = paddle.xcor() + PADDLE_WIDTH_HALF
        self.upper = paddle.ycor() + PADDLE_HEIGHT_HALF
        self.lower = paddle.ycor() - PADDLE_HEIGHT_HALF


class Paddle(turtle.Turtle):

    @staticmethod
    def register_paddle_shape(wnd):
        wnd.register_shape("paddle",
                           ((-PADDLE_WIDTH_HALF, 0),
                            (-PADDLE_WIDTH_HALF, -PADDLE_HEIGHT_HALF),
                            (PADDLE_WIDTH_HALF, -PADDLE_HEIGHT_HALF),
                            (PADDLE_WIDTH_HALF, PADDLE_HEIGHT_HALF),
                            (-PADDLE_WIDTH_HALF, PADDLE_HEIGHT_HALF),
                            (-PADDLE_WIDTH_HALF, 0)))

    def __init__(self, name, environment):
        super().__init__()
        self.name = name
        self.environment = environment
        self.hideturtle()
        self.penup()
        self.speed(0)
        self.shape("paddle")
        self.tilt(90)
        self.color("white")
        # print(f"Paddle {self.name} Shapesize: {self.shapesize()}")

    def borders(self):
        return PaddleBorders(self)

    def sety(self, y):
        if y > 0:
            y = min(y + PADDLE_HEIGHT_HALF, self.environment.upper_border) - \
                PADDLE_HEIGHT_HALF
        else:
            y = max(y - PADDLE_HEIGHT_HALF, self.environment.lower_border) + \
                PADDLE_HEIGHT_HALF
        super().sety(y)

    # noinspection PyMethodOverriding
    def goto(self, x: float, y: float) -> None:
        self.setx(x)
        self.sety(y)
