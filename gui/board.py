from constants import RIGHT_PLAYER, LEFT_PLAYER
from gui.ball import *
from gui.paddle import *
from gui.score_board import ScoreBoard
from manual_player import ManualPlayer
from simple_computer_player import SimpleComputerPlayer

HEIGHT = 600
WIDTH = 800

RIGHT_BORDER = WIDTH / 2
LEFT_BORDER = RIGHT_BORDER * -1
PADDLE_POSITION = (RIGHT_BORDER - 50)

WIDTH_QUARTER = WIDTH / 4


class Environment:
    def __init__(self):
        self.height = HEIGHT
        self.width = WIDTH
        self.right_border = RIGHT_BORDER
        self.left_border = LEFT_BORDER
        self.upper_border = HEIGHT / 2
        self.lower_border = HEIGHT / 2 * -1


class Board:
    def __init__(self, game):
        self.wnd = turtle.Screen()
        self.game = game
        self.environment = Environment()
        self.height = self.environment.height
        self.width = self.environment.width
        self.right_border = self.environment.right_border
        self.left_border = self.environment.left_border
        self.upper_border = self.environment.upper_border
        self.lower_border = self.environment.lower_border
        self.is_quitted = False
        self.init_board()
        self.score_board = ScoreBoard(self)
        Paddle.register_paddle_shape(self.wnd)
        self.paddle_left = Paddle("Left", self.environment)
        self.paddle_right = Paddle("Right", self.environment)
        self.ball = Ball()

        self.player_1 = SimpleComputerPlayer(self.environment,
                                             lambda y: self.paddle_left.sety(y))
        self.player_2 = ManualPlayer(self.wnd, self.environment,
                                     self.paddle_right,
                                     up_key="Up", down_key="Down",
                                     use_mouse=True)

    def init_board(self):
        self.wnd.title("Pong")
        self.wnd.bgcolor("black")
        self.wnd.setup(width=WIDTH + 50, height=HEIGHT + 50)
        self.wnd.onkeypress(self.quit, "q")
        self.wnd.listen()
        self.wnd.cv.bind()

    @staticmethod
    def paint_frame():
        pen = turtle.Turtle()
        pen.hideturtle()
        pen.speed(0)
        pen.penup()
        pen.color("white")
        pen.goto(0 - WIDTH / 2, 0 + HEIGHT / 2)
        pen.pendown()
        pen.forward(WIDTH)
        pen.right(90)
        pen.forward(HEIGHT)
        pen.right(90)
        pen.forward(WIDTH)
        pen.right(90)
        pen.forward(HEIGHT)
        pen.penup()

    def show(self):
        self.paint_frame()
        self.paddle_left.goto(-1 * PADDLE_POSITION, 0)
        self.paddle_right.goto(PADDLE_POSITION, 0)
        self.ball.goto(0, 0)
        self.paddle_left.showturtle()
        self.paddle_right.showturtle()
        self.ball.showturtle()
        self.score_board.update()
        self.wnd.tracer(0)

    def quit(self):
        self.is_quitted = True

    def is_quit(self):
        return self.is_quitted

    def update(self):
        self.ball.setx(self.calculate_x())
        self.ball.sety(self.calculate_y())
        ball_y = self.ball.ycor() if self.ball.xcor() > self.left_border * 0.70\
            else None
        self.player_1.update(self.ball.xcor(), ball_y)
        # self.player_2.update()
        self.wnd.update()

    def calculate_y(self):
        ycor = self.ball.next_ycor()
        if ycor + BALL_RADIUS >= self.upper_border or \
                ycor - BALL_RADIUS <= self.lower_border:
            self.ball.switch_dy()
            return self.ball.ycor()
        return ycor

    def is_ball_y_collision_paddle(self, paddle):
        next_ball_y = self.ball.next_ycor()
        pb = paddle.borders()
        coll = pb.lower - BALL_RADIUS <= next_ball_y <= pb.upper + BALL_RADIUS
        # print("---")
        # print("ball.y: ", self.ball.ycor())
        # print("ball_yn: ", next_ball_y)
        # print("pb.upper: ", pb.upper)
        # print("pb.lower: ", pb.lower)
        # print("coll: ", "True" if coll else "False")
        return coll

    def paddle_ball_offset(self, paddle):
        return abs(self.ball.next_ycor() - paddle.ycor())

    def bounce_angle_factor(self, paddle):
        offset = self.paddle_ball_offset(paddle)
        return 1 + offset * offset * offset / 100000.0

    def is_ball_x_collision_paddle_left_side(self):
        pdl_borders = self.paddle_right.borders()
        next_ball_x = self.ball.next_xcor()
        coll =  self.ball.xcor() + BALL_RADIUS <= pdl_borders.left <= \
                next_ball_x + BALL_RADIUS
        # print("---")
        # print("ball.x: ", self.ball.xcor())
        # print("ball_xn: ", next_ball_x)
        # print("radius: ", BALL_RADIUS)
        # print("pdl-left: ", pdl_borders.left)
        # print("coll: ", "True" if coll else "False")
        return coll

    def is_ball_x_collision_paddle_right_side(self):
        pdl_borders = self.paddle_left.borders()
        next_ball_x = self.ball.next_xcor()
        return next_ball_x - BALL_RADIUS <= pdl_borders.right

    def is_ball_collision_paddle_right(self):
        return self.ball.move_right() and \
               self.is_ball_x_collision_paddle_left_side() and \
               self.is_ball_y_collision_paddle(self.paddle_right)

    def is_ball_collision_paddle_left(self):
        return not self.ball.move_right() and \
               self.is_ball_x_collision_paddle_right_side() and \
               self.is_ball_y_collision_paddle(self.paddle_left)

    def calculate_x(self):
        collision_paddle_right = self.is_ball_collision_paddle_right()
        collision_paddle_left = self.is_ball_collision_paddle_left()
        if collision_paddle_right or collision_paddle_left:
            self.ball.switch_dx()
            self.ball.dy *= self.bounce_angle_factor(self.paddle_right if
                                                     collision_paddle_right
                                                     else self.paddle_left)
            return self.ball.xcor()
        else:
            collision_right = self.is_ball_collision_border_right()
            collision_left = self.is_ball_collision_border_left()

            if collision_right:
                self.game.increment_score(LEFT_PLAYER)
            if collision_left:
                self.game.increment_score(RIGHT_PLAYER)

            if collision_right or collision_left:
                self.ball.setx(0)
                self.ball.switch_dx()
                self.ball.dy = DEFAULT_Y_OFFSET
                self.score_board.update()

        return self.ball.next_xcor()

    def is_ball_collision_border_right(self):
        next_ball_x = self.ball.next_xcor()
        return next_ball_x + BALL_RADIUS >= RIGHT_BORDER

    def is_ball_collision_border_left(self):
        next_ball_x = self.ball.next_xcor()
        return next_ball_x - BALL_RADIUS <= LEFT_BORDER

    def score(self, player):
        return self.game.score(player)

    def finish_game(self, player):
        again = self.score_board.finish_game(player)
        if again:
            self.game.reset_score()
            self.show()
        else:
            quit()
