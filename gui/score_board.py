import turtle

from constants import RIGHT_PLAYER, LEFT_PLAYER

Y_OFFSET_SCORE = 20
Y_OFFSET_FINISH = 80

FONT_SIZE = 100


class Pen(turtle.Turtle):
    def __init__(self, score_board, x, y):
        super().__init__()
        self.score_board = score_board
        self.speed(0)
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(x, y)

    def update(self, score, font_size=FONT_SIZE):
        self.clear()
        self.write("{}".format(score),
                   align="center",
                   font=("Courier", font_size, "normal"))


class ScoreBoard:
    def __init__(self, board):
        self.board = board
        y_offset_score = (FONT_SIZE + Y_OFFSET_SCORE)
        width_quarter = self.board.width / 4
        self.pen_left = Pen(self, self.board.left_border + width_quarter,
                            self.board.upper_border - y_offset_score)
        self.pen_right = Pen(self,
                             self.board.right_border - width_quarter,
                             self.board.upper_border - y_offset_score)

    def update(self):
        self.pen_left.update(self.board.score(LEFT_PLAYER))
        self.pen_right.update(self.board.score(RIGHT_PLAYER))

    def finish_game(self, player):
        self.update()
        y_offset_finish = (FONT_SIZE + Y_OFFSET_FINISH)
        finish_pen = Pen(self, 0, self.board.upper_border-y_offset_finish)
        if player == LEFT_PLAYER:
            player_name = f"Left player wins {self.board.score(LEFT_PLAYER)}:" \
                          f"{self.board.score(RIGHT_PLAYER)}"
        else:
            player_name = f"Right player wins " \
                          f"{self.board.score(RIGHT_PLAYER)}:" \
                          f"{self.board.score(LEFT_PLAYER)}"

        finish_pen.update(player_name, font_size=int(FONT_SIZE / 3))

        again = self.board.wnd.textinput("Question", "Again (Y/N)")
        finish_pen.clear()
        return again is not None and again.lower() == "y"
