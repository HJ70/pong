import turtle

DEFAULT_X_OFFSET = 4
DEFAULT_Y_OFFSET = 4

BALL_RADIUS = 10


class Ball(turtle.Turtle):

    def __init__(self):
        super().__init__()
        self.penup()
        self.hideturtle()
        self.shape("circle")
        self.color("red")
        self.speed(0)
        self.dx = DEFAULT_X_OFFSET
        self.dy = DEFAULT_Y_OFFSET

    def move_right(self):
        return self.dx > 0

    def switch_dx(self):
        self.dx *= -1

    def next_ycor(self):
        return self.ycor() + self.dy

    def next_xcor(self):
        return self.xcor() + self.dx

    def switch_dy(self):
        self.dy *= -1
