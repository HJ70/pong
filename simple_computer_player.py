from player import Player


class SimpleComputerPlayer(Player):
    def __init__(self, environment, paddle_sety_func):
        super().__init__(environment, paddle_sety_func)
        self.paddle_sety_func = paddle_sety_func

    def update(self, ball_x, ball_y):
        if ball_y is not None:
            self.paddle_sety_func(ball_y)

