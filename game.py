import time

from constants import ROUNDS_PER_GAME
from gui.board import Board


class Game:
    def __init__(self):
        self.round_counter = 0
        self.round_speed_factor = 1
        self.machine_speed_factor = self.speed_test()
        self.scores = []
        self.reset_score()
        self.board = Board(self)
        self.board.show()

    @staticmethod
    def speed_test():
        start = time.time_ns()
        for i in range(100000):
            i * i
        end = time.time_ns()
        end_start = end - start
        factor = end_start / 150000000
        return factor

    def run(self):
        while True:
            if self.board.is_quit():
                self.quit()
            self.round_counter += 1
            if self.round_counter % 20 == 0:
                self.round_speed_factor += 1

            sleep_time = self.machine_speed_factor / self.round_speed_factor
            time.sleep(sleep_time)
            self.board.update()

    def reset_score(self):
        self.scores = [0 for _ in range(2)]

    def increment_score(self, player):
        self.round_counter = 0
        self.round_speed_factor = 1
        self.scores[player] += 1
        if self.scores[player] >= ROUNDS_PER_GAME:
            self.board.finish_game(player)
            if self.board.is_quit():
                self.quit()

    def score(self, player):
        return self.scores[player]

    @staticmethod
    def quit():
        print("Bye bye...")
        exit(0)
